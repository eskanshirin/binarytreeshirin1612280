/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenfogel.binarytree.implementation;

import java.util.Comparator;

/**
 *generic comparator method 
 * @author Shirin Eskandari
 */
public class CustomComparator <T extends Comparable<T>> implements Comparator<T>
{
   public int compare(T x, T y)
   {
        return  x.compareTo(y);
   }
}
