package com.kenfogel.binarytree.implementation;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 * 
 * @author Ken Fogel,Shirin Eskandari
 */
public class BinaryTreeNode<T extends Comparable<?>> {

    // package access to support reading and writing directly
    // from the BinaryTree class without the need for setters & getters
     BinaryTreeNode<T> left;
     BinaryTreeNode<T> right;
     T data;
    
    /**
     * Constructor that creates nodes
     * @param newData 
     */
    public BinaryTreeNode(T newData) {
        this.left = null;
        this.right = null;
       this.data = newData;
      
    }
    public BinaryTreeNode(T newdata, BinaryTreeNode<T> leftnew, BinaryTreeNode<T> rightnew)
      {
         left = leftnew;
         right = rightnew;
         data = newdata;
      }
    
}
