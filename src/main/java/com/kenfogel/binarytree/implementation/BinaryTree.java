package com.kenfogel.binarytree.implementation;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel,Shirin Eskandari
 */
public class BinaryTree<T extends Comparable<T>> {

    // Root node reference. Will be null for an empty tree.
    private BinaryTreeNode<T> root;
    private Comparator<T> comparator;

    /**
     * Creates an empty binary tree -- a null root reference.
     */
    public BinaryTree() {
        root = null;
        comparator = null;
    }

    public BinaryTree(T value) {
        root = new BinaryTreeNode<T>(value);
    }

    public BinaryTree(Comparator<T> comp) {
        root = null;
        comparator = comp;
    }

    private int compare(T x, T y) {
        if (comparator == null) {
            return x.compareTo(y);
        } else {
            return comparator.compare(x, y);
        }
    }

    /**
     * Inserts the given data into the binary tree.Uses a recursive helper.
     *
     * @param data
     */
    public void insert(T data) {
        root = insert(root, data);
    }

    private BinaryTreeNode<T> insert(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            node = new BinaryTreeNode<T>(data);
        } else {
            if (compare(data, node.data) < 0) {
                node.left = insert(node.left, data);
            } else {
                node.right = insert(node.right, data);
            }
        }

        return (node); // in any case, return the new reference to the caller
    }

    /**
     * Returns true if the given target is in the binary tree.Uses a recursive
     * helper.
     *
     * @param data
     * @return true of false depending on whether the data is found
     */
    public boolean lookup(T data) {
        return (lookup(root, data));
    }

    /**
     * Recursive lookup -- given a node, recur down searching for the given
     * data.
     */
    private boolean lookup(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            return (false);
        }

        if (compare(data, node.data) == 0) {
            return (true);
        } else if (compare(data, node.data) < 0) {
            return (lookup(node.left, data));
        } else {
            return (lookup(node.right, data));
        }
    }

  
    public int size() {
        return (size(root));
    }

    private int size(BinaryTreeNode<T> node) {
        if (node == null) {
            return (0);
        } else {
            return (size(node.left) + 1 + size(node.right));
        }
    }

    /**
     * Returns the max root-to-leaf depth of the tree. Uses a recursive helper
     * that recurses down to find the max depth.
     *
     * @return The depth of the tree from the root to the lowest node
     */
    public <T extends Comparable<?>> int maxDepth() {
        return (maxDepth(root));
    }

    private <T extends Comparable<?>> int maxDepth(BinaryTreeNode<T> node) {
        if (node == null) {
            return (0);
        } else {
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);

            // use the larger + 1 
            return (Math.max(lDepth, rDepth) + 1);
        }
    }

    /**
     * Returns the min value in a non-empty binary search tree. Uses a helper
     * method that iterates to the left to find the min value.
     *
     * @return The smallest value in the tree
     */
    public T minValue() {
        return (minValue(root));
    }

    /**
     * Finds the min value in a non-empty binary search tree.
     */
    private T minValue(BinaryTreeNode<T> node) {
        BinaryTreeNode<T> current = node;
        while (current.left != null) {
            current = current.left;
        }

        return (current.data);
    }

    /**
     * Prints the node values in the "inorder" order. Uses a recursive helper to
     * do the traversal.
     */
    public void printInorderTree() {
        printInorderTree(root);
        System.out.println();
    }

    private <T extends Comparable<?>> void printInorderTree(BinaryTreeNode<T> node) {
        if (node == null) {
            return;
        }

        // left, node itself, right 
        printInorderTree(node.left);
        System.out.print(" " + node.data);
        printInorderTree(node.right);
    }

    /**
     * Prints the node values in the "postorder" order. Uses a recursive helper
     * to do the traversal.
     */
    public void printPostorder() {
        printPostorder(root);
        System.out.println();
    }

    private void printPostorder(BinaryTreeNode<T> node) {
        if (node == null) {
            return;
        }

        // first recur on both subtrees 
        //   System.out.print(" " +node.data );
        printPostorder(node.left);
        printPostorder(node.right);

        // then deal with the node 
        System.out.print(node.data + "  ");
    }

    /**
     * Given a binary tree, prints out all of its root-to-leaf paths, one per
     * line. Uses a recursive helper to do the work.
     */
    public void printPaths() {
        List<BinaryTreeNode> path = new ArrayList<BinaryTreeNode>();
        //  printPaths(root, path, 0);
    }

    /**
     * Recursive printPaths helper -- given a node, and an array containing the
     * path from the root node up to but not including this node, prints out all
     * the root-leaf paths.
     */
    public void printPaths(BinaryTreeNode<T> node, T[] path, int pathLen) {
        if (node == null) {
            return;
        }

        // append this node to the path array 
        path[pathLen] = node.data;
        pathLen++;

        // it's a leaf, so print the path that led to here 
        if (node.left == null && node.right == null) {
            printArray(path, pathLen);
            //   System.out.print(node.data + "  ");
        } else {
            // otherwise try both subtrees 
            printPaths(node.left, path, pathLen);
            printPaths(node.right, path, pathLen);
        }
    }

    /**
     * Utility that prints ints from an array on one line.
     */
    private void printArray(T[] ints, int len) {
        int i;
        for (i = 0; i < len; i++) {
            System.out.print(ints[i] + " ");
        }
        // System.out.println();
    }

    /**
     * it gets all nodes in levels and pass the level and maxdepth and one node 
     * to printshirin method to print binarytree
     * Prints out each level in the tree from left to right. Uses a recursive
     * helper to do the work.
     */
    public void printLineByLine() {
        int maxLevel = maxDepth();
        List<List<BinaryTreeNode>> levels = printLineByLine(root);
        for (List<BinaryTreeNode> level2 : levels) {
            printshirin(level2, 1, maxLevel);
        }
    }
/**
 * this method gets level and one node and calculate edges and the places it should 
 * put spaces and calls printwhitespace method to put spaces between nodes
 * and recursively calls own method till all level done and print 
 * @param <T>
 * @param level2
 * @param level
 * @param maxLevel 
 */
    private <T extends Comparable<?>> void printshirin(List<BinaryTreeNode> level2, int level, int maxLevel) {
        int edge = maxLevel - level;
        int edges = (int) Math.pow(2, (Math.max(edge - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (edge)) - 1;
        int betweenSpaces = (int) Math.pow(2, (edge + 1)) - 1;
        printWhitespaces(firstSpaces);

        List<BinaryTreeNode> newNodes = new ArrayList<BinaryTreeNode>();
        for (BinaryTreeNode node : level2) {
            if (node != null) {
                System.out.print(node.data);
                newNodes.add(node.left);
                newNodes.add(node.right);
            } else {
                newNodes.add(null);
                newNodes.add(null);
                System.out.print(" ");
            }

            printWhitespaces(betweenSpaces);
        }
        System.out.println("");
        for (int i = 1; i <= edges; i++) {
            for (int j = 0; j < level2.size(); j++) {
                printWhitespaces(firstSpaces - i);
                if (level2.get(j) == null) {
                    printWhitespaces(edges + edges + i + 1);
                    continue;
                }
                if (level2.get(j).left != null) {
                    System.out.print("/");
                } else {
                    printWhitespaces(1);
                }

                printWhitespaces(i + i - 1);

                if (level2.get(j).right != null) {
                    System.out.print("\\");
                } else {
                    printWhitespaces(1);
                }
                printWhitespaces(edges + edges - i);
            }

            System.out.println("");
        }
        printshirin(newNodes, level + 1, maxLevel);

    }

    private void printWhitespaces(int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(" ");
        }
    }
/**
 * this method makes list of list of the nodes due to keep track of all nodes and their childs
 * in seprate levels . 
 * @param <T>
 * @param root
 * @return 
 */
    private <T extends Comparable<?>> List<List<BinaryTreeNode>> printLineByLine(BinaryTreeNode<T> root) {

        if (root == null) {
            return Collections.emptyList();
        }
        List<List<BinaryTreeNode>> levels = new LinkedList<>();
        final Queue<BinaryTreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            List<BinaryTreeNode> level = new ArrayList<>(queue.size());
            levels.add(level);
            for (BinaryTreeNode node : new ArrayList<>(queue)) {
                int depth = maxDepth();
                level.add(node);
                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {

                    queue.add(node.right);
                }
                queue.poll();
            }
        }
        return levels;
    }

}
